package com.example.android.facebookloginsample.objects;

import java.util.Date;

public class Product {
//DB Columns
    public static final String PRODUCT_ID = "Product_id";
    public static final String PRODUCT_NAME = " Name";
    public static final String DATE = " Date";
    public static final String PRODUCT_PRICE = " Price";
    public static final String PRODUCT_DESCRIPTION = " Description";
    public static final String PRODUCT_QUANTITY = " Quantity";
    public static final String TOTAL_PRICE = " Total price";



    public static int Id;
    public  String  Name;
    public String Date;
    public  String Price;
    public  String Description;
    public String Quantity;
    public String Totalprice;


 public static int  getId(){
     return Id ;
 }
    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription( String description) {
        Description = description;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getTotalprice() {
        return Totalprice ;
    }

    public void setTotalprice(String totalprice) {
        Totalprice = Totalprice;
    }
}
