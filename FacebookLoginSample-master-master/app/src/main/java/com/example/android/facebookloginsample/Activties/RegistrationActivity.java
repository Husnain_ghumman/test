package com.example.android.facebookloginsample.Activties;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.android.facebookloginsample.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationActivity extends Activity  {
    // Variable Declaration should be in onCreate()
    private Button mSubmit;
    private Button mCancel;

    private EditText et_fname;
    private EditText et_lname;
    private EditText et_pass;
    private EditText et_cpass;
    private EditText et_email;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        context=this;
        initViews();
        setUpListeners();

        //Assignment of UI fields to the variables

    }

    private void setUpListeners() {
        mCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), com.example.android.facebookloginsample.Activties.LoginActivity.class);
                startActivity(i);
                // finish();
            }
        });

        mSubmit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String fname = et_fname.getText().toString();
                if (fname.equals("")) {
                    // invalid = true;
                    et_fname.setError(" Enter First Name");
                } else {
                    et_fname.setError(null);
                }

                String lname = et_lname.getText().toString();
                if (lname.equals("")) {
                    // invalid = true;
                    et_lname.setError(" Enter last Name");
                } else {
                    et_lname.setError(null);
                }
                String pass = et_pass.getText().toString();
                if (pass.equals("")) {
                    // invalid = true;
                    et_pass.setError(" Enter password");
                }
                String pass1 = et_pass.getText().toString();
                if (pass1.equals("")) {
                    // invalid = true;
                    et_cpass.setError(" Confirm Password ");
                } else if (pass != pass1) {
                    et_cpass.setError(" MissMatch password ");
                } else {
                    et_cpass.setError(null);
                }
                String email = et_email.getText().toString().trim();
                if (email.equals("")) {
                    // invalid = true;
                    et_email.setError("Enter Valid Email");
                }
                if (!isValid(email)) {
                    // invalid = true;
                    et_email.setError("Enter valid Email");
                } else {
                    Intent i_register = new Intent(RegistrationActivity.this, com.example.android.facebookloginsample.Activties.LoginActivity.class);
                    startActivity(i_register);
                    //finish();
                }

            }
        });

    }
    private void initViews() {
        et_fname = (EditText) findViewById(R.id.et_fname);
        et_lname = (EditText) findViewById(R.id.et_lname);

        et_pass = (EditText) findViewById(R.id.et_pass);
        et_cpass = (EditText) findViewById(R.id.et_cpass);

        et_email = (EditText) findViewById(R.id.et_email);
        mSubmit = (Button) findViewById(R.id.submit);
        mCancel = (Button) findViewById(R.id.cancel);
    }

    public static boolean isValid(String email)
    {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches())
        {
            return true;
        }
        else{
            return false;
        }



    }


}



