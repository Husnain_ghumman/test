package com.example.android.facebookloginsample.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.android.facebookloginsample.objects.Product;

import java.util.ArrayList;
import java.util.List;

import static com.example.android.facebookloginsample.Database.DBHelper.DATABASE_TABLE_PRODUCT;
import static com.example.android.facebookloginsample.objects.Product.DATE;
import static com.example.android.facebookloginsample.objects.Product.Id;
import static com.example.android.facebookloginsample.objects.Product.PRODUCT_DESCRIPTION;
import static com.example.android.facebookloginsample.objects.Product.PRODUCT_ID;
import static com.example.android.facebookloginsample.objects.Product.PRODUCT_NAME;
import static com.example.android.facebookloginsample.objects.Product.PRODUCT_PRICE;

public class DatabaseOperations {
    static DBHelper db;
    public static Context mContext;


    public DatabaseOperations(Context context) {
        if (db == null) {
            db = new DBHelper(context);
        }

        mContext = context;
    }

    public Product addRowForProduct(Product product) {
        SQLiteDatabase dbHelper = db.getWritableDatabase();
        ContentValues values = getContentValuesForProduct(product, false);
        long i = dbHelper.insert(DATABASE_TABLE_PRODUCT, null, values);
        Log.d("Response", "" + i);
        if (i != -1) {
            product.setId((int) i);
            return product;
        }
        return product;
        //returns row id
    }

    public void deleteRowProduct(Product product) {
        SQLiteDatabase dbHelper = db.getWritableDatabase();
        dbHelper.delete(DATABASE_TABLE_PRODUCT, Product.getId() + "=" + Product.getId(), null);
    }

    public Product updateRowForProduct(Product product) {
        SQLiteDatabase dbHelper = db.getWritableDatabase();
        ContentValues values = getContentValuesForProduct(product, true);
        int i = dbHelper.update(DATABASE_TABLE_PRODUCT, values, Product.getId() + "=" + Product.getId(), null);
        if (i != -1) {
            product.setId((int) i);
            return product;
        }
        return product;
    }

    private ContentValues getContentValuesForProduct(Product product, boolean isupdate) {
        ContentValues values = new ContentValues();
        if (isupdate) {
            values.put(PRODUCT_ID, Product.getId());
        }
        values.put(PRODUCT_NAME, product.getName());
        // values.put(Product.COL_EMAIL, exerciseProblemObject.getEmail());
        values.put(PRODUCT_PRICE, product.getPrice());
        values.put(DATE, String.valueOf(product.getDate()));
        values.put(PRODUCT_DESCRIPTION, product.getDescription());
       // values.put(Product.PRODUCT_QUANTITY, product.getQuantity());
       // values.put(product.Totalprice, product.getTotalprice());
        // values.put(Program.COL_IDENTIFY_GOALS_TIME, exerciseProblemObject.getGoals_time());
        // values.put(Program.COL_IDENTIFY_GOALS, exerciseProblemObject.getGoals());
        //  values.put(Program.COL_AEROBIC_EXERCISES, exerciseProblemObject.getStr_aerobic_exercises());
        // values.put(Program.COL_STRENGTH_EXERCISES, exerciseProblemObject.getStr_strengthExercises());
        // values.put(Program.COL_FLEXIBILITY_EXERCISES, exerciseProblemObject.getStr_flexibilityExercises());
        // values.put(Program.COL_CUSTOMIZED_EXERCISES, exerciseProblemObject.getStr_customizedExercises());
        return values;
    }

    public ArrayList<Product> getAllProductRecordsInList(final boolean isTemplate) {
        ArrayList<Product> list = new ArrayList<Product>();
        SQLiteDatabase dbHelper = db.getReadableDatabase();
        String[] cols = {PRODUCT_ID, PRODUCT_NAME, PRODUCT_PRICE, PRODUCT_DESCRIPTION, DATE};

        Cursor cursor = dbHelper.query(DATABASE_TABLE_PRODUCT, cols, null, null, null, null, null, "PRODUCT_ID");
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Product product = new Product();
                product.setId(Integer.valueOf(cursor.getString(cursor.getColumnIndex(cols[0]))));
                product.setName(cursor.getString(cursor.getColumnIndex(cols[1])));
                product.setPrice(cursor.getString(cursor.getColumnIndex(cols[1])));
                product.setDescription(cursor.getString(cursor.getColumnIndex(cols[2])));
               // product.setTotalprice(cursor.getString(cursor.getColumnIndex(cols[4])));
               // product.setDate(cursor.getString(cursor.getColumnIndex(cols[4])));
                //program.setType(cursor.getString(cursor.getColumnIndex(cols[4])));
                //program.setStr_identify_problems(cursor.getString(cursor.getColumnIndex(cols[5])));
                //program.setGoals_time(cursor.getString(cursor.getColumnIndex(cols[6])));
                //program.setGoals(cursor.getString(cursor.getColumnIndex(cols[7])));
                //program.setStr_aerobic_exercises(cursor.getString(cursor.getColumnIndex(cols[8])));
                //program.setStr_strengthExercises(cursor.getString(cursor.getColumnIndex(cols[9])));
                //program.setStr_flexibilityExercises(cursor.getString(cursor.getColumnIndex(cols[10])));
                // program.setStr_customizedExercises(cursor.getString(cursor.getColumnIndex(cols[11])));
                //if(!isTemplate && program.getType().equalsIgnoreCase("Client") || isTemplate && program.getType().equalsIgnoreCase("Template")){
                //  list.add(program);
            }
        }
        cursor.close();

        return list;
    }

   // public ArrayList<Product> getProductData(Product product) {
     //   ArrayList<Product> Products = new ArrayList<Product>();
       // String query = "SELECT " + Product.PRODUCT_ID + ","
         //       + Product.PRODUCT_NAME + "," + DBHelper.DATABASE_TABLE_PRODUCT
           //     + product.PRODUCT_PRICE + "," + " FROM "
             //   + DBHelper.DATABASE_TABLE_PRODUCT;

       // return Products;
    //}


}