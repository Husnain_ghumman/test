package com.example.android.facebookloginsample.Activties;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.facebookloginsample.Database.DBHelper;
import com.example.android.facebookloginsample.PrefUtils;
import com.example.android.facebookloginsample.R;
import com.example.android.facebookloginsample.objects.User_object;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends Activity {
    private CallbackManager callbackManager;
    private LoginButton loginButton;
     Button buton_home;
    private TextView btnLogin;
    private ProgressDialog progressDialog;
    Button button_Login;
    Button button_SignUp;
    EditText et_Email;
    EditText et_pass;
    User_object User;
    DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        db = new DBHelper(this);
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        buton_home = (Button)findViewById(R.id.button_home);
        buton_home.setOnClickListener(new  View.OnClickListener(){
            public void onClick(View v ){
                Intent i = new Intent(LoginActivity.this,HomeActivity.class);
                startActivity(i);
            }

        });
        Button mSignUp = (Button) findViewById(R.id.register);
        mSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(i);

            }
        });
        button_Login = (Button) findViewById(R.id.loginButton);

        button_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_Email = (EditText) findViewById(R.id.email);
                String Email = et_Email.getText().toString();
                if (!isValid(Email)) {
//                    invalid = true;
                    et_Email.setError("Enter valid Email");
                } else {
                    et_Email.setError(null);
                }
                EditText et_Pass = (EditText)findViewById(R.id.pass);
                String password = et_Pass.getText().toString();
                if (password.equals("")) {
                    et_Pass.setError("Enter your Password");
                } else {
                    et_Pass.setError(null);
                }
            }
        });

        if(PrefUtils.getCurrentUser(LoginActivity.this) != null){

            Intent homeIntent = new Intent(LoginActivity.this, LogoutActivity.class);

            startActivity(homeIntent);

            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


        callbackManager=CallbackManager.Factory.create();

        loginButton= (LoginButton)findViewById(R.id.login_button);

        loginButton.setReadPermissions("public_profile", "email","user_friends");

        btnLogin= (TextView) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();

                loginButton.performClick();

                loginButton.setPressed(true);

                loginButton.invalidate();

                loginButton.registerCallback(callbackManager, mCallBack);

                loginButton.setPressed(false);

                loginButton.invalidate();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            progressDialog.dismiss();

            // App code
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {

                            Log.e("response: ", response + "");
                                try {
                                    User = new User_object();
                                    User.facebookID = object.getString("id").toString();
                                    User.email = object.getString("email").toString();
                                    User.name = object.getString("name").toString();
                                    //user.gender = object.getString("gender").toString();
                                    PrefUtils.setCurrentUser(User,LoginActivity.this);

                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                              Toast.makeText(LoginActivity.this,"welcome "+User.name,Toast.LENGTH_LONG).show();
                              Toast.makeText(LoginActivity.this,"facebook id "+User.facebookID,Toast.LENGTH_LONG).show();
                              Toast.makeText(LoginActivity.this,"Email  "+User.email,Toast.LENGTH_LONG).show();
                              Intent intent=new Intent(LoginActivity.this,LogoutActivity.class);
                              startActivity(intent);
                              finish();

                        }

                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            progressDialog.dismiss();
        }

        @Override
        public void onError(FacebookException e) {
            progressDialog.dismiss();
        }
    };

    public static boolean isValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }}

}
