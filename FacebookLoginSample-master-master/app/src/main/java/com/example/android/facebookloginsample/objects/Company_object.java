package com.example.android.facebookloginsample.objects;

public class Company_object {
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "Name";
    public static final String KEY_ADDRESS = "Address";
    public static final String KEY_EMAIL = "Email";
    public  int _id;
    private   String Name;
    public  String Address;
    public  String Email;

    public  int  get_id(){
    return _id;
}
    public void set_id(int _id) {
        this._id = _id;
    }

    public  String get_Name(String Name) {
        return Name;
    }

    public void set_Name(String name) {
        Name = name;
    }

    public  String get_Address(String Address) {
        return Address;
    }

    public void set_Address(String address) {
        Address = address;
    }

    public String get_Email(String Email) {
        return Email;
    }

    public void set_Email(String email) {
        Email = email;
    }
}
