package com.example.android.facebookloginsample.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.android.facebookloginsample.objects.Product;
import com.example.android.facebookloginsample.objects.Sales_object;

public class DBHelper extends SQLiteOpenHelper {
    private SQLiteDatabase db;
   // private SQLiteDatabase Db;
    private static final String DATABASE_NAME = "Contacts.db";
    private static final int DATABASE_VERSION =1;
    public static final String DATABASE_TABLE_NAME = "company_Table";
    public static final String DATABASE_TABLE_PRODUCT = "Product_Table";
    public static final String DATABASE_TABLE_SALE = "Sales_Table";

     DBHelper DB ;
    // Database creation sql statement
    private static final String DATABASE_TABLE_CREATE =
            "CREATE TABLE " + DATABASE_TABLE_NAME + "(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT,"+
                    "Name TEXT NOT NULL, Address TEXT NOT NULL,Email TEXT NOT NULL);";

    private static final String TABLE_CREATE =
            "CREATE TABLE " + DATABASE_TABLE_PRODUCT + "(" +
                    "Product_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " Date , Name TEXT NOT NULL ,Price TEXT NOT NULL ,Description TEXT NOT NULL  );";
    private static final String DATABASE_TABLE=
            "CREATE TABLE " + DATABASE_TABLE_SALE + "(" +
                    "Sales_id INTEGER PRIMARY KEY AUTOINCREMENT,"+
                    " Product_id TEXT NOT NULL,TotalSales TEXT NOT NULL);";
    // join query
   // private  static final String SALES_PRODUCT_JOIN  = "SELECT PRODUCT_ID,PRODUCT_NAME,PRODUCT_PRICE,PRODUCT_DESCRIPTION,DATE M PRODUCT_TABLE  " +
                                             //  "INNER JOIN " +
                                            //   "Sales_Table" +FROM
                                            //   "WHERE " +
                                             //  "PRODUCT_TABLE.Product_id = SALES_TABLE.Sales_id";

    // fatch query
   // private  static  final String FATCH_QUERY =  "SELECT * PRODUCT_TABLE && SALES_TABLE WHERE PRODUCT_ID,PRODUCT_NAME," +
                                               //  "PRODUCT_PRICE,PRODUCT_DESCRIPTION,Sales_id" +
                                                 //"WHERE [Condition]";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
           SQLiteDatabase db = this.getWritableDatabase();
          // SQLiteDatabase Db = this.getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
            db.execSQL(DATABASE_TABLE_CREATE);
            db.execSQL(TABLE_CREATE);
            db.execSQL(DATABASE_TABLE);
           // db.execSQL(MY_QUERY);
           // Db.execSQL(FATCH_QUERY);
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS DATABASE_TABLE_NAME");
        db.execSQL("DROP TABLE IF EXISTS DATABASE_TABLE_PRODUCT");
        db.execSQL("DROP TABLE IF EXISTS DATABASE_TABLE_SALE");
        // oncreate
        onCreate(db);
    }

}

