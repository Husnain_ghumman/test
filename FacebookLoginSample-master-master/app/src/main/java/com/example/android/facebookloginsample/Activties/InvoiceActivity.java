package com.example.android.facebookloginsample.Activties;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.android.facebookloginsample.Database.DatabaseOperations;
import com.example.android.facebookloginsample.R;
import com.example.android.facebookloginsample.objects.Product;

import java.text.SimpleDateFormat;


public class InvoiceActivity extends Activity {

    private Context context;
     private EditText  et_price;
    private EditText  et_date;
     private EditText  et_quantity;
     private Button    b_addmore;
    private EditText  et_name;
    private  EditText  et_description;
    private EditText et_totalprice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
        context=this;
        initViews();
       setUpListeners();
    }

    private void setUpListeners() {
        b_addmore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Product product = new Product();
                product.setPrice(et_price.getText().toString());
                product.setQuantity(et_quantity.getText().toString());
                product.setDescription(et_description.getText().toString());
                product.setName(et_name.getText().toString());
                product.setTotalprice(et_totalprice.getText().toString());
                product.setDate(et_date.getText().toString().trim());
                DatabaseOperations databaseOperations = new DatabaseOperations(context);
                databaseOperations.addRowForProduct(product);

               /* product = new Product();
                product.setQuantity(et_quantity.getText().toString());
                databaseOperations = new DatabaseOperations(context);
                databaseOperations.addRowForProduct(product);
                product = new Product();
                product.setDescription(et_description.getText().toString());
                databaseOperations = new DatabaseOperations(context);
                databaseOperations.addRowForProduct(product);*/
                //  product = new Product();
                // product.setDate(et_date.);
                //databaseOperations = new DatabaseOperations(context);
                //databaseOperations.addRowForProduct(product);

                    }
                });
            }
    private void initViews() {
        et_description= (EditText) findViewById(R.id.et_description);
        et_price= (EditText) findViewById(R.id.et_price);
        et_quantity = (EditText) findViewById(R.id.et_quantity);
        et_name = (EditText) findViewById(R.id.et_name);
        et_totalprice =(EditText)findViewById(R.id.et_totalprice);
        et_date = (EditText) findViewById(R.id.et_date);
       // SimpleDateFormat sdf = new SimpleDateFormat( "yyyy/MM/dd" );
       // String dob_var=sdf.format(et_date.getText().toString());
        b_addmore =(Button)findViewById(R.id.b_addmore);
    }
}