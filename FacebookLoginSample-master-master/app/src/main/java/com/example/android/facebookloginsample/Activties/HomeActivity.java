package com.example.android.facebookloginsample.Activties;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.android.facebookloginsample.R;


public class HomeActivity extends Activity{
    Button Button_Sales;
    Button Button_Statistics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        initViews();
        setUpListeners();

    }

    private void setUpListeners() {
        Button_Sales.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent i = new Intent(getBaseContext(),InvoiceActivity.class);
                    startActivity(i);
                    // finish();
                }
            });
    }

    private void initViews() {
        Button_Sales = (Button)(findViewById(R.id.Button_Sales));
        Button_Statistics = (Button)(findViewById(R.id.Button_Statistics));
    }

}
