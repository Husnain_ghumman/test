package com.example.android.facebookloginsample.objects;


public class Sales_object {
    // fields of sales table
    public static final String DATE = "Date";
    public static final String PRODUCTS_ID = " Sales_id";
    public static final String TOTAL_Sales = "  Total Sales";

    public  int id_;
    public double price;
    public  String Date;
    public  String Product;
    public  double   TotalPrice;
    public int quantity;


    public void setId_(int id_) {
        this.id_ = id_;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getProduct() {
        return Product;
    }

    public void setProduct(String product) {
        Product = product;
    }

    public double getTotalPrice() {
         TotalPrice =quantity* price;
        return TotalPrice;
    }

}
